package testngMaven;

import org.testng.annotations.Test;

public class TestMaven2 {

	@Test(groups = {"smoke","regression"})
	public void sampleTest2()
	{
		System.out.println("I am in TestMaven2 with smoke and regression!!");
	}
	
	@Test(groups = {"smoke"})
	public void sampleTestSmoke()
	{
		System.out.println("I am in TestMaven2 with smoke test only!!");
	}
	
	@Test(groups = {"regression"})
	public void sampleTestRegression()
	{
		System.out.println("I am in TestMaven2 with regression!!");
	}
}
